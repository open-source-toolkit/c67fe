# 麒麟Linux下编译tcpdump所需的源文件

本仓库提供了在麒麟Linux系统下编译tcpdump所需的四个源文件。这些文件包含了编译过程中必要的依赖项和工具，确保您能够顺利完成tcpdump的编译工作。

## 资源文件列表

以下是本仓库提供的四个源文件：

1. **bison-3.8.tar.xz**  
   Bison是一个语法分析器生成器，用于生成解析器代码。在编译tcpdump时，Bison是必需的工具之一。

2. **flex-2.6.4.tar.gz**  
   Flex是一个快速词法分析器生成器，用于生成词法分析器代码。在编译tcpdump时，Flex也是必需的工具之一。

3. **libpcap-1.10.4.tar.gz**  
   libpcap是一个用于网络流量捕获的库。tcpdump依赖于libpcap来捕获和分析网络数据包。

4. **tcpdump-4.99.4.tar.gz**  
   tcpdump是一个强大的网络分析工具，用于捕获和分析网络数据包。本仓库提供的源文件包含了tcpdump的最新版本。

## 使用说明

1. **下载源文件**  
   您可以通过本仓库的下载链接获取上述四个源文件。

2. **解压文件**  
   使用以下命令解压各个源文件：
   ```bash
   tar -xf bison-3.8.tar.xz
   tar -xf flex-2.6.4.tar.gz
   tar -xf libpcap-1.10.4.tar.gz
   tar -xf tcpdump-4.99.4.tar.gz
   ```

3. **编译安装**  
   按照以下步骤依次编译和安装各个工具和库：
   ```bash
   # 编译并安装bison
   cd bison-3.8
   ./configure
   make
   sudo make install

   # 编译并安装flex
   cd ../flex-2.6.4
   ./configure
   make
   sudo make install

   # 编译并安装libpcap
   cd ../libpcap-1.10.4
   ./configure
   make
   sudo make install

   # 编译并安装tcpdump
   cd ../tcpdump-4.99.4
   ./configure
   make
   sudo make install
   ```

4. **验证安装**  
   安装完成后，您可以通过以下命令验证tcpdump是否成功安装：
   ```bash
   tcpdump --version
   ```

## 注意事项

- 在编译过程中，请确保您的系统已经安装了必要的编译工具链（如gcc、make等）。
- 如果遇到依赖问题，请根据错误提示安装相应的依赖库。

## 贡献

如果您在使用过程中遇到任何问题或有改进建议，欢迎提交Issue或Pull Request。

## 许可证

本仓库中的源文件遵循其各自的许可证。请参考各个源文件的LICENSE文件以获取更多信息。